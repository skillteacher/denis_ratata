using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammopikapt : MonoBehaviour
{
    [SerializeField] private int amount = 10;
    

    private void OnTriggerEnter(Collider other)
    {
        Ammo ammo = other.gameObject.GetComponentInChildren<Ammo>();
        if (!ammo) return;
        ammo.AddAmmo(amount);
        Destroy(gameObject);
    }
}
