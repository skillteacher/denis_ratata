using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyAI : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Animator animator;
    [SerializeField] private float damage = 25f;
    [SerializeField] private float viewRange = 10f;
    [SerializeField] private enemyAnima enemyAnima; 
    private NavMeshAgent navMeshAgent;
    private bool isProvoked;
    private playerHeat playerHeat;

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        playerHeat = target.GetComponent<playerHeat>();
    }

    private void Update()
    {
        float distanceToTarger = Vector3.Distance(target.position, transform.position);

        EventTracking(distanceToTarger);

        if (isProvoked)
        {
            EngageTarget(distanceToTarger);
        }
    }

    private void EventTracking(float distance)
    {
        if (distance <= viewRange)
        {
            isProvoked = true;
            animator.SetBool("move", true);
        }
    }

    private void EngageTarget(float distance)
    {
        if(distance > navMeshAgent.stoppingDistance)
        {
            Chase();
        }
        else
        {
            Attack();
        }
    }

    private void Chase()
    {
        navMeshAgent.SetDestination(target.position);
        animator.SetBool("attack", false);
    }

    private void Attack()
    {
        animator.SetBool("attack", true);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, viewRange);
    }
    private void OnEnable()
    {
        enemyAnima.AnimationAttackMoment += TargetAttack;
    }

    private void OnDisable()
    {
        enemyAnima.AnimationAttackMoment -= TargetAttack;
    }

    public void TargetAttack()
    {
        playerHeat.TakeDamage(damage);
    }
}
