using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHeat : MonoBehaviour
{
    [SerializeField]private float hitPoints = 100f;

     public void TakeDamage(float damageAmount)
    {
        hitPoints -= damageAmount;

        if(hitPoints<= 0)
        {
            Destroy(gameObject); 
        }
    }
}
