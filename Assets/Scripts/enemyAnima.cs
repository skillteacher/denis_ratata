using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyAnima : MonoBehaviour
{
    
    public event Message AnimationAttackMoment;

    public void AnimationAttack()
    {
        AnimationAttackMoment?.Invoke();
    }
}

public delegate void Message();