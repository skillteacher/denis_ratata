using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathHandler : MonoBehaviour
{
    [SerializeField] private Canvas gameOverCanvas;
    [SerializeField] private playerHeat playerHeat;

    private void Start()
    {
        gameOverCanvas.enabled = false;
        Time.timeScale = 1f;
    }

    private void OnEnable()
    {
        playerHeat.PlayerDeath += OnPlayerDeath;
    }

    private void OnDisable()
    {
        playerHeat.PlayerDeath -= OnPlayerDeath;
    }

    private void OnPlayerDeath()
    {
        gameOverCanvas.enabled = true;
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

    }
}
