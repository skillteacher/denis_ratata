using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private float range = 100f;
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float damage = 10f;
    [SerializeField] private ParticleSystem muzzleFlashEffect;
    [SerializeField] private float sparksLifetime = 0.1f;
    [SerializeField] private GameObject sparksPrefab;
    [SerializeField] private Ammo ammo;
    private void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            Fire();
        }
    }

    private void Fire()
    {
        if (!ammo.IsAmmoEnough()) return;
        ammo.ReduceAmmoByOne();
        PlayMuzzleEffect();
        Raycasting();
    }

    private void PlayMuzzleEffect()
    {
        
    }

    private void Raycasting()
    {
        RaycastHit hit;
        if (!Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, range)) return;
        HitEffect(hit.point);
        EnemyHeat enemyHeat = hit.transform.GetComponent<EnemyHeat>();
        if (enemyHeat)
        {
            enemyHeat.TakeDamage(damage);
        }
    }
    private void HitEffect(Vector3 point)
    {
       var sparks = Instantiate(sparksPrefab, point, Quaternion.identity);
        Destroy(sparks, sparksLifetime);
    }
    
}
















































































